# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-31 06:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supa', '0003_remove_office_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='department',
            name='ministry',
        ),
        migrations.RemoveField(
            model_name='office',
            name='department',
        ),
        migrations.DeleteModel(
            name='Department',
        ),
        migrations.DeleteModel(
            name='Ministry',
        ),
    ]
